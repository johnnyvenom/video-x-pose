Landmarks: 

0 - nose
1 - left eye (inner)
2 - left eye
3 - left eye (outer)
4 - right eye (inner)
5 - right eye
6 - right eye (outer)
7 - left ear
8 - right ear
9 - mouth (left)
10 - mouth (right)
11 - left shoulder
12 - right shoulder
13 - left elbow
14 - right elbow
15 - left wrist
16 - right wrist
17 - left pinky
18 - right pinky
19 - left index
20 - right index
21 - left thumb
22 - right thumb
23 - left hip
24 - right hip
25 - left knee
26 - right knee
27 - left ankle
28 - right ankle
29 - left heel
30 - right heel
31 - left foot index
32 - right foot index

These landmarks connect to:

segment_names = [
  "head",
  "shoulders",
  "waist",
  "torso left",
  "torso right",
  "left upper arm",
  "right upper arm",
  "left forearm",
  "right forearm",
  "left hand",
  "right hand",
  "left upper leg",
  "right upper leg",
  "left lower leg",
  "right lower leg",
  "left foot",
  "right foot" 
]

segment_connections = [
  [ [0,1], [1,2],[2,3],[3,7],[0,4],[4,5],[5,6],[6,8] ],
  [11,12],
  [23,24],
  [11,23],
  [12,24],
  [11,13],
  [12,14],
  [13,15],
  [14,16],
  [ [15,17],[17,19],[19,15],[15,21] ],
  [ [16,18],[18,20],[20,16],[16,22] ],
  [23,25],
  [24,26],
  [25,27],
  [26,28],
  [ [27,29],[29,31],[31,27] ],
  [ [28,30],[30,32],[28,32] ],
]


segment[0] = {
  id: i,
  name: segment_names[i],
  connections: segment_connections[i]
  status: "show"
}


0 head
1 shoulders
2 waist
3 torso left
4 torso right
5 left upper arm
6 right upper arm
7 left forearm
8 right forearm
9 left hand
10 right hand
11 left upper leg
12 right upper leg
13 left lower leg
14 right lower leg
15 left foot
16 right foot

<span>face</span>
<span>shoulders</span>
<span>waist</span>
<span>torso left</span>
<span>torso right</span>
<span>left upper arm</span>
<span>right upper arm</span>
<span>left forearm</span>
<span>right forearm</span>
<span>left hand</span>
<span>right hand</span>
<span>left upper leg</span>
<span>right upper leg</span>
<span>left lower leg</span>
<span>right lower leg</span>
<span>left foot</span>
<span>right foot</span>
