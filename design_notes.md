# controls

## Tools panel

### View source  
- ✅ View  
  - Text: toggle between camera input and 3D scene. 
  - New text: toggle between Video and Pose views
  - Type: radio buttons
### Video options
- ✅ Mirror view
  - Type: toggle
  - Description: switch between normal and reversed video view
- ✅ Show markers
  - Type: toggle
  - Description: draw markers at joints
- ✅ Show segments
  - Type: toggle
  - Description: draw lines between joints
### Pose options
-Scene
  - Mirror view
  - ✅ Zoom view in/out (Z-axis)
  - Zoom to selected
  - Rotate around subject (X-axis)
  - ✅ Auto-rotate start/stop
  - Auto rotate speed
  - Auto-rotate direction
  - ✅ Reset view
- Manipulations
  - Draw segments between markers
  - ✅ Adjust marker size
  - Adjust segment size 
  - Select/deselect markers
  - Traces: create
  - Traces: select markers
  - Traces: length

## Video controls panel

- Record
- Play
- Pause 
- Stop
- Playback speed
- Loop
- Set start point
- Set end point
- Save new visualization (*maybe manage in files panel*)
- Switch to live-stream mode (*maybe manage in files panel*)

## File management

- Each file: 
  - Load
  - Rename
  - Delete
