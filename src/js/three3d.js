import * as THREE from "three";
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';

import { markers, segments, showGL, reverse, changeCamX, changeCamZ, changeMkrSize, showPlane, viewAxes, autoRotate, worldCoords } from "./tools.js";
import { poseLandmarks, poseWorldLandmarks } from './pose.js';
import { updateDims, aspectRatio } from './camera.js';

let markersPrev, segmentsPrev; // we'll use these to check if user has changed status

let camera, scene, axesHelper, planeHelper;
let colorMkr, colorMkrTrace, colorMkrMissing;
let colorSeg, colorSegTrace, colorSegMissing;
export let controls;
let geometry, matMkrShow, matMkrMissing, matMkrHide; 
let matSegShow, matSegMissing, matSegHide; 

export let renderer;

let landmarks = [];
const threeMarkers = [];
const threeVectors = []; // array of THREE.vector3 for xyz marker positions
const threeSegGeometry = [];
const threeSegments = [];

let camPosX = 0;
let camPosZ = 200;
let mkrSize = 1.5;
let ax = false;
let ap = false;
let ar = false;


init();
initMarkers();
initSegments();
initHelpers();
animate();

function init() {
  scene = new THREE.Scene();
  camera = new THREE.PerspectiveCamera(75, 1.25, 0.1, 1000);
  renderer = new THREE.WebGLRenderer();

  renderer.setSize( updateDims(aspectRatio).w, updateDims(aspectRatio).h );
  document.getElementById('view_pose').appendChild(renderer.domElement);

  camera.position.z = camPosZ;

  // const light = new THREE.PointLight( 0x00FF44 );
  // const light2 = new THREE.PointLight( 0xFF0000 );
  // light.position.set( 10, 0, 100 );
  // light2.position.set( -10, 0, 50 );
  // light2.power = 75;
  // scene.add( light );
  // scene.add( light2 );

  controls = new OrbitControls( camera, renderer.domElement );
  controls.listenToKeyEvents( window ); // optional

  // controls.enableDamping = true; // an animation loop is required when either damping or auto-rotation are enabled
  controls.autoRotate = false;
  controls.dampingFactor = 0.05;

  controls.screenSpacePanning = false;

  controls.minDistance = 100;
  controls.maxDistance = 500;

  controls.keys = {
    LEFT: 'ArrowLeft', //left arrow
    UP: 'ArrowUp', // up arrow
    RIGHT: 'ArrowRight', // right arrow
    BOTTOM: 'ArrowDown' // down arrow
  }
}

function initMarkers() {

  // initialize markers
  colorMkr = new THREE.Color(0x3388ff);
  colorMkrTrace = new THREE.Color(0x13b54e);
  colorMkrMissing = new THREE.Color(0x777777);

  geometry = new THREE.SphereGeometry(mkrSize, 16, 16);
  matMkrShow = new THREE.MeshBasicMaterial({ color: colorMkr });
  matMkrMissing = new THREE.MeshBasicMaterial({ color: colorMkrMissing });
  matMkrHide = new THREE.MeshBasicMaterial({ transparent: true, opacity: 0 });

  for (let i = 0; i < markers.length; i++) {
    threeMarkers[i] = new THREE.Mesh( geometry, matMkrShow );
    threeVectors[i] = new THREE.Vector3();

    scene.add( threeMarkers[i]);
  }

  // console.log(threeVectors)

}

function initSegments() {
  colorSeg = new THREE.Color(0xfc4646);
  colorSegTrace = new THREE.Color(0x75c492);
  colorSegMissing = new THREE.Color(0x777777);

  matSegShow = new THREE.LineBasicMaterial({
    color: colorSeg,
    linewidth: 5,
    linecap: 'round',
    linejoin: 'round'
  }); 
  matSegMissing = new THREE.LineBasicMaterial({
    color: colorSegMissing,
    linewidth: 20,
    linecap: 'round',
    linejoin: 'round' 
  }); 
  matSegHide = new THREE.LineBasicMaterial({
    transparent: true, 
    opacity: 0 
  }); 

  // iterate through all 18 segments.
  for (let i = 0; i < segments.length; i++) {
    
    // an empty array that will hold the threeVectors (the actual point coordinates) for each point of this segment
    const connections = []; 

    // iterate through points of the segments...
    for (let j = 0; j < segments[i].connections.length; j++) {
      // ...and put the threeVector coords in the array
      connections.push(threeVectors[segments[i].connections[j]]);
    }

    // create the GEOMETRY from point to point to point coordinates.
    threeSegGeometry[i] = new THREE.BufferGeometry().setFromPoints(connections);

    // create a LINE with the GEOMETRY and MATERIAL
    threeSegments[i] = new THREE.Line( threeSegGeometry[i], matSegShow);
    
    scene.add( threeSegments[i]);
  }
  // console.log(threeSegGeometry);

}

function initHelpers() {
  const plane = new THREE.Plane( new THREE.Vector3( 0, 1, 0 ), 120 );
  planeHelper = new THREE.PlaneHelper( plane, 400, 0xffff00 );

  axesHelper = new THREE.AxesHelper(200);

  const planeGeometry = new THREE.PlaneGeometry( 3, 3, 1, 1);
  const ground = new THREE.Mesh( planeGeometry,
    new THREE.MeshPhongMaterial( {
      color: 0xa0adaf, shininess: 10 } ) );
  ground.rotation.x = - Math.PI / 2;
  ground.scale.multiplyScalar( 3 );
  ground.receiveShadow = true;
  // scene.add( ground );
}

function updateMesh() {

}


function animate() {

  requestAnimationFrame( animate );

  let scale = 40;

  // add floor plane to the scene
  // (need to update to plane mesh!)
  if (showPlane && !ap) {
    scene.add(planeHelper);
    ap = true;
    // console.log("ap true");
  } else if (!showPlane && ap) {
    scene.remove(planeHelper);
    ap = false;
    // console.log("ap false");
  } else {
    // console.log("ap");
    // do nothing...
  };

  // show rotation axes in scene
  if (viewAxes && !ax) {
    scene.add(axesHelper);
    ax = true;
  } else if (!viewAxes && ax) {
    scene.remove(axesHelper);
    ax = false;
  } else {
    // do nothing...
  };

  // auto rotate camera around avatar
  if (autoRotate) {
    if (!controls.autoRotate) {
      controls.autoRotate = true;
    }
  } else {
    if (controls.autoRotate) {
      controls.autoRotate = false;
    }
  }

  // rotate camera with onscreen slider (not working currently...)
  // if (changeCamX != camPosX) {
  //   camPosX = changeCamX;
  //   let xaxis = new THREE.Vector3( 1, 0, 0 );
  //   camera.rotateOnWorldAxis(xaxis, camPosX*10);
  //   console.log(camera.rotation);
  // }

  // change camera position
  if (changeCamZ != camPosZ) {
    camPosZ = changeCamZ;
    camera.position.z = camPosZ;
  }

  // change marker size
  if (changeMkrSize != mkrSize) {
    mkrSize = changeMkrSize;
    geometry = new THREE.SphereGeometry(mkrSize, 8, 4);
    for (let i = 0; i < 33; i++) {
      threeMarkers[i].geometry = geometry; 
    }
  }

  // use local or world coordinates
  // always use local.. for now
  if (worldCoords) {
    landmarks = poseWorldLandmarks;
  } else {
    landmarks = poseLandmarks;
  }


  if (showGL) {

    let tracked = [];

    for (let i = 0; i < markers.length; i++) {

      // to check: when would landmarks[x] be undefined? 
      if (landmarks[i] != undefined) {

        //set threeMarkers position in webGL scene 
        let x = (landmarks[i].x * 2 - 1) * scale;
        let y = (landmarks[i].y * -2 + 1) * scale + 100;
        let z = (landmarks[i].z / 2) * scale;
        threeVectors[i].set(x, y, z);
        threeMarkers[i].position.x = threeVectors[i].x;
        threeMarkers[i].position.y = threeVectors[i].y;
        threeMarkers[i].position.z = threeVectors[i].z;

        if (landmarks[i].visibility > 0.5) {
          tracked[i] = true; 
        } else {
          tracked[i] = false;
        }

        switch (markers[i].status) {
          case "show":
            if (tracked[i]) {
              threeMarkers[i].material = matMkrShow;
            } else {
              threeMarkers[i].material = matMkrMissing;
            }
            break;
          case "hide":
            threeMarkers[i].material = matMkrHide;
            break;
          case "trace":
            console.log("trace coming soon");
            break;
          default:
            break;
        }
      }
    }


    const trackedSegs = [];
    // iterate through all 18 segments.
    for (let i = 0; i < segments.length; i++) {
      
      // an empty array that will hold the threeVectors (the actual point coordinates) for each point of this segment
      const connections = []; 

      trackedSegs[i] = true;
      // iterate through points of the segments...
      for (let j = 0; j < segments[i].connections.length; j++) {
        // ...and put the threeVector coords in the array
        connections.push(threeVectors[segments[i].connections[j]]);
        
        if(tracked[segments[i].connections[j]] == false) {
          trackedSegs[i] = false;
        }
      }

      // create the GEOMETRY from point to point to point coordinates.
      threeSegGeometry[i].setFromPoints(connections);

      // create a LINE with the GEOMETRY and MATERIAL
      threeSegments[i].geometry = threeSegGeometry[i];

      switch (segments[i].status) {
        case "show":
          if (trackedSegs[i]) {
            threeSegments[i].material = matSegShow;
          } else {
            threeSegments[i].material = matSegMissing;
          }
          break;
        case "hide":
          threeSegments[i].material = matSegHide;
          break;
        case "trace":
          console.log("trace coming soon");
          break;
        default:
          break;
      }
    }

    controls.update(); // only required if controls.enableDamping = true, or if controls.autoRotate = true

    renderer.render( scene, camera );
  }
}