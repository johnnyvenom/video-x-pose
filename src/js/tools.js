import { controls } from "./three3d.js";

export let showImage = true;
export let showGL = false;
export let reverse = false;
export let showMarkers = false;
export let showSegments = false;
export let changeCamX = 0; // not working currently
export let changeCamZ = 200;
export let changeMkrSize = 2;
export let viewAxes = false;
export let showPlane = false;
export let autoRotate = false;
export let worldCoords = false;

// controlling video or pose views
const viewSources = document.querySelectorAll('input[name="viewer_view"]');

for (const viewSource of viewSources) {
  viewSource.addEventListener('change', (e) => {
    if (viewSource.value === "Video") { // show video input
      showImage = true;
      showGL = false;
      for (const v of document.getElementsByClassName("vid_view")) {
        v.classList.remove("hidden");
      };
      for (const p of document.getElementsByClassName("pose_view")) {
        p.classList.add("hidden");
      }
    } else { // show webGL output
      showImage = false;
      showGL = true;
      for (const p of document.getElementsByClassName("pose_view")) {
        p.classList.remove("hidden");
      }
      for (const v of document.getElementsByClassName("vid_view")) {
        v.classList.add("hidden");
      }
    }
  });
};

// video display controls
const vidCtlMirror = document.getElementById("vid_view_mirror");
const vidCtlMarkers = document.getElementById("vid_view_markers");
const vidCtlSegments = document.getElementById("vid_view_segments");

vidCtlMirror.addEventListener("change", (e) => {
  if (vidCtlMirror.checked) {
    reverse = true; 
  } else {
    reverse = false;
  } 
});

vidCtlMarkers.addEventListener("change", (e) => {
  if (vidCtlMarkers.checked) {
    showMarkers = true;
  } else {
    showMarkers = false;
  }
});

vidCtlSegments.addEventListener("change", (e) => {
  if (vidCtlSegments.checked) {
    showSegments = true;
  } else {
    showSegments = false;
  }
});

//webGL display controls
// const poseCtlX = document.getElementById("pose_view_X"); 
const poseCtlZ = document.getElementById("pose_view_z");
const poseViewReset = document.getElementById("pose_view_reset");
const poseMkrSize = document.getElementById("pose_marker_size");
const poseAxes = document.getElementById("pose_view_axes");
const posePlane = document.getElementById("pose_view_plane");
const poseCamAR = document.getElementById("pose_view_autorotate");
const poseCoords = document.getElementById("pose_view_world");

// Rotate camera on X axis
// poseCtlX.addEventListener("input", (e) => {
//   changeCamX = poseCtlX.value;
// })

// Zoom camera on Z axis
poseCtlZ.addEventListener("input", (e) => {
  changeCamZ = poseCtlZ.value;
})

poseViewReset.addEventListener("mousedown", (e) => {
  controls.reset();
  changeMkrSize = 1;
  poseMkrSize.value = 1;
});

poseAxes.addEventListener("change", (e) => {
  if (poseAxes.checked) {
    viewAxes = true;
  } else {
    viewAxes = false;
  }
});

// Marker size.
poseMkrSize.addEventListener("input", (e) => {
  changeMkrSize = poseMkrSize.value;
  console.log(changeMkrSize);
})

// TESTING / DEVELOPMENT TOOLS
// Auto-rotate (for debugging landmark Z coordinates)
poseCamAR.addEventListener("change", (e) => {
  if (poseCamAR.checked) {
    autoRotate = true;
  } else {
    autoRotate = false;
  }
});

// poseCoords.addEventListener("change", (e) => {
//   if (poseCoords.checked) {
//     worldCoords = true;
//   } else {
//     worldCoords = false;
//   }
// });

posePlane.addEventListener("change", (e) => {
  if (posePlane.checked) {
    showPlane = true;
  } else {
    showPlane = false;
  }
});

// marker and segment setup 
export const markers = []; // array of marker objects
export const segments = []; // array of segment objects

function createMarkerArray() {
  const markerNames = [];
  const markerModalGroups = document.getElementsByClassName("markers-modal-group");

  // get the markers names in an array
  for (const markerModalGroup of markerModalGroups) {
    const markerNameSpans = markerModalGroup.getElementsByTagName("span");
    for (const span of markerNameSpans) {
      markerNames.push(span.innerText);
    }
  }

  for (let i = 0; i < markerNames.length - 1; i++) {
    markers[i] = {
      "id": i,
      "name": markerNames[i],
      "status": "show"
    }

    // update marker.status on user interaction
    const buttonGroupName = "m" + i;
    const radioOptions = document.querySelectorAll(`input[name="${buttonGroupName}"]`);

    for (const radioOption of radioOptions) {
      radioOption.addEventListener('change', (e) => {
        // console.log(`Old status: ${markers[i].status}`);
        markers[i].status = radioOption.value;
        // console.log(`New status: ${markers[i].status}`);
        
        // removed "Select ALL" checked when others have been checked.
        const allSel = document.querySelectorAll(`input[name="m33"]`);
        for (const each of allSel) {
          each.checked = false;
        }
      });
    };

  }

  // "Select ALL" options: resets the buttons, and updates status of all markers
  let selAllMkrs = [];

  for (let i = 0; i < 3; i++) {
    const z = ["s", "t", "h"];
    const y = ["show", "trace", "hide"];

    selAllMkrs[i] = document.querySelector(`input[id=${"m33-"+z[i]}]`);

    selAllMkrs[i].addEventListener('change', (e) => {
      for (let j = 0; j < markers.length; j++) {
        const buttonId = "m" + j + "-" + z[i];
        const b = document.querySelector(`input[id=${buttonId}]`);
        b.checked = true;
        markers[j].status = y[i];
      }
    });

  }
  
}

function createSegmentArray() {
  const segmentConnections = [
    // [ [0,1], [1,2],[2,3],[3,7],[0,4],[4,5],[5,6],[6,8] ],
    [7,3,2,1,0,9,10,0,4,5,6,8],
    [11,12],
    [23,24],
    [11,23],
    [12,24],
    [11,13],
    [12,14],
    [13,15],
    [14,16],
    [15,17,19,15,21],
    // [ [15,17],[17,19],[19,15],[15,21] ],
    [16,18,20,16,22],
    // [ [16,18],[18,20],[20,16],[16,22] ],
    [23,25],
    [24,26],
    [25,27],
    [26,28],
    [27,29,31,27],
    // [ [27,29],[29,31],[31,27] ],
    [28,30,32,28]
    // [ [28,30],[30,32],[28,32] ],
  ];
  const segmentNames = [];
  const segmentModalGroups = document.getElementsByClassName("segments-modal-group");
  
  // get the segment names in an array
  for (const segmentModalGroup of segmentModalGroups) {
    const segmentNameSpans = segmentModalGroup.getElementsByTagName("span");
    for (const span of segmentNameSpans) {
      segmentNames.push(span.innerText);
    }
  };

  // console.log(segmentNames);
  // add segment objects to array
  for (let i = 0; i < segmentNames.length - 1; i++) {
    segments[i] = {
      "id": i,
      "name": segmentNames[i],
      "connections": segmentConnections[i],
      "status": "show"
    }

    // update segment.status on user interaction
    const buttonGroupName = "s" + i;
    const radioOptions = document.querySelectorAll(`input[name="${buttonGroupName}"]`);

    for (const radioOption of radioOptions) {
      radioOption.addEventListener('change', (e) => {
        // console.log(`Old status: ${segments[i].status}`);
        segments[i].status = radioOption.value;
        // console.log(`New status: ${segments[i].status}`);
        
        // removed "Select ALL" checked when others have been checked.
        const allSel = document.querySelectorAll(`input[name="s17"]`);
        for (const each of allSel) {
          // console.log(each);
          each.checked = false;
        }
      });
    };
  }

  // "Select ALL" options: resets the buttons, and updates status of all segments
  let selAllSgmts = [];

  for (let i = 0; i < 3; i++) {
    const z = ["s", "t", "h"];
    const y = ["show", "trace", "hide"];

    selAllSgmts[i] = document.querySelector(`input[id=${"s17-"+z[i]}]`);

    selAllSgmts[i].addEventListener('change', (e) => {
      for (let j = 0; j < segments.length; j++) {
        const buttonId = "s" + j + "-" + z[i];
        const b = document.querySelector(`input[id=${buttonId}]`);
        b.checked = true;
        segments[j].status = y[i];
      }
    });
  }
}

createMarkerArray();
createSegmentArray();


