import * as MP_CAM from "@mediapipe/camera_utils";
import * as MP_DRAW from "@mediapipe/drawing_utils";
import * as MP_POSE from "@mediapipe/pose";

// <video>, <canvas> and c2d canvas context for pose to use
import { videoElement, canvasElement, ctx } from "./camera.js";
// tools
import { showImage, showMarkers, showSegments, reverse } from "./tools.js";

export let poseLandmarks = [];
export let poseWorldLandmarks = [];
let mpInput;

// this Pose object will give us the pose data. 
const pose = new MP_POSE.Pose({locateFile: (file) => {
  return `https://cdn.jsdelivr.net/npm/@mediapipe/pose/${file}`;
}});

// const pose = new MP_POSE.Pose({
//   locateFile: (file) => {
//     return `./mppose/${file}`;
//   }
// });

// pose options
pose.setOptions({
  modelComplexity: 1,
  smoothLandmarks: true,
  enableSegmentation: true,
  smoothSegmentation: true,
  minDetectionConfidence: 0.5,
  minTrackingConfidence: 0.5
});

export async function videoInput(live) {

  if (live) {
    // the mediapose way of capturing frames
    mpInput = new MP_CAM.Camera(videoElement, {
      onFrame: async () => {
        await pose.send({image: videoElement});
      }
    });

    mpInput.start();
  } else {
    console.log("playing back!");
    mpInput.stop();
    // from recorded video
    videoElement.onloadeddata = async () => {
      pose.reset();
      await pose.send({ image: videoElement });
    }
  };
}





// start the web camera and send frames to mediapose
videoInput(true);
// mpInput.start();

// call this function as soon as pose data is returned
function onResults(results) {
  ctx.save();
  
  if (reverse) {
    ctx.translate(canvasElement.width, 0);
    ctx.scale(-1, 1);
  }

  ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);

  
  if (showImage) {
    ctx.globalCompositeOperation = 'destination-atop';
    ctx.drawImage(results.image,0,0, canvasElement.width, canvasElement.height);
  };

  if (showMarkers) { 
    // canvasCtx.globalCompositeOperation = 'destination-atop';
    ctx.globalCompositeOperation = 'source-over';
    MP_DRAW.drawLandmarks(ctx, results.poseLandmarks, {color: '#fff', lineWidth: 0.5});
  }

  if (showSegments) { 
    // canvasCtx.globalCompositeOperation = 'destination-atop';
    ctx.globalCompositeOperation = 'source-over';
    MP_DRAW.drawConnectors(ctx, results.poseLandmarks, MP_POSE.POSE_CONNECTIONS, {color: '#eeeeee', lineWidth: 2});
  }
  
  ctx.restore();
  
  poseLandmarks = results.poseLandmarks;
  poseWorldLandmarks = results.poseWorldLandmarks;
}

pose.onResults(onResults);