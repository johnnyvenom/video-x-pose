import { DateTime, Interval } from 'luxon';
// import * as noUiSlider from 'nouislider';
// import 'nouislider/dist/nouislider.css';

import { showGL } from './tools.js';

let canvasStream;
let mediaRecorder;
const blobs = [];
const blobUrls = [];
let videoIndex = 0;
let recordedChunks = [];
let startTime, stopTime, elapsedTime;

// output_canvas contains the mediaPipe output
// view_pose child is the webGL canvas. 
const mpCanvas = document.getElementById('output_canvas');
const wglCanvas = document.getElementById('view_pose').firstElementChild;
const startButton = document.getElementById('record');
const stopButton = document.getElementById('stop');
const videoList = document.getElementById('video_list');

startButton.addEventListener('click', startRecording);
stopButton.addEventListener('click', stopRecording);

const maxNumberOfVideos = 8;
maxFiles = document.getElementById("maxFiles"); 
maxFiles.innerText = maxNumberOfVideos;

async function startRecording() {
  if (videoList.children.length >= maxNumberOfVideos) {
    alert("You have recorded the maximum allowed number of videos. You can delete some to make more space.\n\nYou can download your videos before deleting from the app if you want to keep them. ");
  } else {
    try {
      let stream;
      if (showGL) {
        stream = wglCanvas.captureStream();
      } else {
        stream = mpCanvas.captureStream();
      }
      canvasStream = new MediaStream(stream.getVideoTracks());
      // videoStream = await navigator.mediaDevices.getUserMedia({ video: true, audio: true });
      // videoElement.srcObject = videoStream;
      
      mediaRecorder = new MediaRecorder(canvasStream, { mimeType: 'video/mp4' });
      mediaRecorder.ondataavailable = handleDataAvailable;
      mediaRecorder.start();
      
      startButton.disabled = true;
      stopButton.disabled = false;
      
      startTime = DateTime.now();
      document.getElementById("recording-status").style.display = "block";
      
    } catch (error) {
      console.error('Error starting recording: ', error);
    }
  }
}

async function stopRecording() {
  // stop the recording
  mediaRecorder.stop();
  canvasStream.getTracks().forEach(track => track.stop());

  // change button states
  startButton.disabled = false;
  stopButton.disabled = true;

  stopTime = DateTime.now();
  document.getElementById("recording-status").style.display = "none";

  // restart the video stream ... or whatever needs to happen here! 
  // videoInput(true);

  mediaRecorder.onstop = () => {
    blobs[videoIndex] = new Blob(recordedChunks, { type: 'video/mp4' });
    blobUrls[videoIndex] = URL.createObjectURL(blobs[videoIndex]);
    createVideoButtonGroup(blobUrls[videoIndex]);
    recordedChunks = [];
  }
}

function handleDataAvailable(event) {
  if (event.data.size > 0) {
    recordedChunks.push(event.data);
  }
}

function createVideoButtonGroup(videoUrl) {
  //the main video button 'videoButton'
  // videoUrl = URL.createObjectURL(blob);
  console.log(`blob: ${videoUrl}\nIndex: ${videoIndex}`);
  const timestamp = DateTime.now().toFormat('hh-mm-ss');
  const elapsedTime = Interval.fromDateTimes(startTime, stopTime).length();
  const vlMin = Math.floor(elapsedTime/60000);
  const vlSec = Math.round(elapsedTime/1000) % 60;
  const videoLen = `${vlMin}m${vlSec}s`;
  const videoName = (`video${('00'+videoIndex).slice(-2)}_${timestamp}_${videoLen}`);
  
  // div containing button cluster for this video file 
  const videoFileUI = document.createElement('div');
  videoFileUI.classList.add('btn-group-vertical', 'video-button-group');

  const videoButton = document.createElement('button');
  videoButton.classList.add('btn', 'btn-light', 'btn-sm');
  // videoButton.classList.add('btn-light');
  videoButton.textContent = videoName;
  videoButton.addEventListener('click', (e) => {
    const newTab = window.open('', '_blank');
    newTab.document.write(`
      <!DOCTYPE html>
      <html>
      <head>
        <link href="https://vjs.zencdn.net/8.3.0/video-js.css" rel="stylesheet">
        <style>
          body { margin: 10px; background: #222; display: flex; align-items: center; justify-content: center}
          #player { }
        </style>
      </head>
      <body>
        <video id="player" class="video-js vjs-default-skin" controls>
          <source src="${videoUrl}" type="video/mp4">
        </video>
        <script src="https://vjs.zencdn.net/8.3.0/video.min.js"></script>
        <script>
          const wHeight = window.innerHeight - 20;
          const wWidth = window.innerWidth - 20; 
          let vHeight;
          let vWidth;
          if (wHeight > wWidth * 0.75) {
            vHeight = wWidth * 0.75;
            vWidth = wWidth;
          } else {
            vWidth = wHeight * 1.25;
            vHeight = wHeight; 
          }
          videojs('player', { controls: true, autoplay: 'muted', loop: true, playbackRates: [0.2, 0.5, 0.75, 1, 1.5, 2], height: vHeight, width: vWidth});
        </script>
      </body>
      </html>
    `);
  });
  
  videoIndex ++;
  
  //download button
  const downloadButton = document.createElement('button');
  downloadButton.classList.add('btn', 'btn-warning', 'btn-sm');
  downloadButton.textContent = "Download";
  downloadButton.addEventListener('click', (e) => {
    const a = document.createElement('a');
    a.href = videoUrl;
    a.download = `${videoName}.mp4`;
    a.click();
  });
    
  //delete button
  const deleteButton = document.createElement('button');
  deleteButton.classList.add('btn', 'btn-danger', 'btn-sm');
  deleteButton.textContent = "Delete";
  deleteButton.addEventListener('click', (e) => {
    URL.revokeObjectURL(videoUrl);
    videoFileUI.remove();
  });

  //group for the donwload/delete buttons
  const btnGroup = document.createElement('div');
  btnGroup.classList.add('btn-group');
  btnGroup.setAttribute('role', 'group');
  // btnGroup.appendChild(openButton); //add to group
  btnGroup.appendChild(downloadButton); //add to group
  btnGroup.appendChild(deleteButton); //add to group


  videoFileUI.appendChild(videoButton); //add video button
  videoFileUI.appendChild(btnGroup); //add download/delete buttons

  //add the button cluster to the sidebar div. 
  videoList.appendChild(videoFileUI);
}

function playInNewTab() {
  const newTab = window.open('', '_blank');
  newTab.document.write(`
    <!DOCTYPE html>
    <html>
    <head>
      <link href="https://vjs.zencdn.net/8.3.0/video-js.css" rel="stylesheet">
      <style>
        body { margin: 10px; background: #222; display: flex; align-items: center; justify-content: center}
        #player { }
      </style>
    </head>
    <body>
      <video id="player" class="video-js vjs-default-skin" controls>
        <source src="${blobUrls[videoIndex]}" type="video/mp4">
      </video>
      <script src="https://vjs.zencdn.net/8.3.0/video.min.js"></script>
      <script>
        const wHeight = window.innerHeight - 20;
        const wWidth = window.innerWidth - 20; 
        let vHeight;
        let vWidth;
        if (wHeight > wWidth * 0.75) {
          vHeight = wWidth * 0.75;
          vWidth = wWidth;
        } else {
          vWidth = wHeight * 1.25;
          vHeight = wHeight; 
        }
        videojs('player', { controls: true, autoplay: 'muted', loop: true, playbackRates: [0.2, 0.5, 0.75, 1, 1.5, 2], height: vHeight, width: vWidth});
      </script>
    </body>
    </html>
  `);
}

// This section is for the range slider - not currently used but can or will be used for the visualizer timeline and setting start/end points. 
// var slider = document.getElementById('slider');

// noUiSlider.create(slider, {
//     start: [20, 80],
//     connect: true,
//     range: {
//         'min': 0,
//         'max': 100
//     }
// });

