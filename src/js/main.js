// Import all of Bootstrap's JS
import * as bootstrap from 'bootstrap'

import { canvasElement, aspectRatio, updateDims } from './camera';
import { renderer } from './three3d';

window.addEventListener("load", (e) => {
  if (window.screen.width < 900 ) {
    alert("This application requires a display width of at least 900px to display properly. Plase try the app on a computer or larger tablet in landscape orientation.")
  }
})


function updateSize() {
  console.log(`update size`);
  // window_height = window.innerHeight-control_height;
  canvasElement.setAttribute("width", updateDims(aspectRatio).w);
  canvasElement.setAttribute("height", updateDims(aspectRatio).h);
  renderer.setSize( updateDims(aspectRatio).w, updateDims(aspectRatio).h );

}

// updateSize();

window.addEventListener("resize", updateSize);

function checkBrowserAndDisplayVideoMimeTypes() {
  const videoElement = document.createElement('video');
  // const canPlayWebm = videoElement.canPlayType('video/webm');
  // const canPlayMp4 = videoElement.canPlayType('video/mp4');
  // console.log(canPlayWebm);
  // console.log(canPlayMp4);

  const supportedTypes = {
    mp4: videoElement.canPlayType('video/mp4; codecs="avc1.42E01E"'),
    webm: videoElement.canPlayType('video/webm; codecs="vp8, vorbis"'),
    ogg: videoElement.canPlayType('video/ogg; codecs="theora"')
  };

  const browser = {
    isChrome: !!window.chrome && !!window.chrome.webstore,
    isFirefox: typeof InstallTrigger !== 'undefined',
    isEdge: !!window.navigator.userAgent.match(/Edge\/(\d+)/),
    isSafari: /^((?!chrome|android).)*safari/i.test(navigator.userAgent),
    isOpera: (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0,
    isIE: !!document.documentMode || !!window.StyleMedia,
  };


  let browserId;

  if (browser.isChrome) {
    browserId = 'Chrome';
  } else if (browser.isFirefox) {
    browserId = 'Firefox';
    alert("It looks like you are using Firefox. Unfortunately recording is not currently available with this browser.\n\nIf possible, switch to another browser for full application functionality. We recommend Safari, Chrome, or Brave for the best compatibility.");
  } else if (browser.isEdge) {
    browserId = 'Edge';
  } else if (browser.isSafari) {
    browserId = 'Safari';
  } else if (browser.isOpera) {
    browserId = 'Opera';
  } else if (browser.isIE) {
    browserId = 'Internet Explorer';
  } else {
    browserId = 'Unknown';
  }


  
  const msg = (`Browser: ${browserId}\n\nSupported video MIME types:\nMP4: ${supportedTypes.mp4}\nWebM: ${supportedTypes.webm}\nOgg: ${supportedTypes.ogg}`);
  // console.log('Supported video MIME types:');
  // console.log('MIME Type: ' + supportedTypes);
  // alert(msg);
}

checkBrowserAndDisplayVideoMimeTypes()

// disable trace option until it is finished.
const traceButtons = document.getElementsByClassName("optTrace");
for (const btn of traceButtons) {
  btn.style.display = "none";
}