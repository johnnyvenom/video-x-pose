// This file contains the JS for initializing the camera and canvas elements. 

// element for camera input
export const videoElement = document.getElementById('input_video');
// element for video and basic post output (not 3D) 
export const canvasElement = document.getElementById('output_canvas');
// 2d canvas context to draw to
export const ctx = canvasElement.getContext('2d'); 

// set initial viewer dimensions
export let canvas_w, canvas_h;
export let aspectRatio = 0.75;

// get width of viewer element. 
let viewspace;

// a utility function to update dimensions if viewer aspect ratio changes
export function updateDims(ar) {
  let dims = {};

  viewspace = document.getElementById("viewer");
  let w = viewspace.offsetWidth;
  let h = viewspace.offsetHeight;

  console.log(`viewer dims: ${w}x${h}`);

  if (h < w * ar) { // camera aspect ratio is narrower than viewer
    dims.h = h*0.99;
    dims.w = (h / ar)*0.99;
  } else { // camera aspect ratio is wider than viewer
    dims.w = w;
    dims.h = w * ar;
  }
  return dims;
}


window.onload = () => {
  canvasElement.setAttribute("width", updateDims(aspectRatio).w);
  canvasElement.setAttribute("height", updateDims(aspectRatio).h);
};

// Get camera dimensions when available;
// Update viewer size if needed
export let streaming = false;

// this happens when the video element has an event "canplay"
// we'll get the camera dimensions and set the corresponding video and canvas element dimensions.
videoElement.addEventListener(
  "canplay", 
  async (event) => {

    // why do we need to check if streaming already? I guess so it only runs once..
    if (!streaming) {

      let stream = await navigator.mediaDevices.getUserMedia({video: true});
      let {width, height} = stream.getTracks()[0].getSettings();

      let cameraAspectRatio = height/width;
      // console.log(`new aspect ratio: ${cameraAspectRatio}`);
      
      if (cameraAspectRatio != aspectRatio) {
        aspectRatio = cameraAspectRatio;
        canvasElement.setAttribute("width", updateDims(aspectRatio).w);
        canvasElement.setAttribute("height", updateDims(aspectRatio).h);
      }  
      streaming = true;
    }
  }, 
  false
);