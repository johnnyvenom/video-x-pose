const path = require('path')

export default {
  base: '/video-x-pose/',
  root: path.resolve(__dirname, 'src'),
  build: {
    outDir: '../public',
    commonjsOptions: { include: [] },
  },
  optimizeDeps: {
    disabled: false,
  },
  server: {
    port: 8080,
    hot: true
  }
}
